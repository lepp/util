# util
This repo contains infos, scripts, and links that (hopefully) will make your daily computing experience more pleasant.

## Terminal multiplexers
Terminal multiplexers are programs that you can run inside an SSH session.
They provide the following enhancements:
- Don't lose work on a connection failure.
- Disconnect and reconnect a session at will.
- Open multiple terminals on a single SSH connection

### Caveats
Most terminal multiplexers don't play well with X forwarding.

### Most popular
This (non-exhaustive) list contains the most popular terminal multiplexers sorted by ease of use.
- byobu (https://byobu.org/)
- tmux (https://tmux.github.io/)
- screen (https://www.gnu.org/software/screen/)

## Fewer password prompts for SSH connections
The script `setup_ssh_control.sh` in this repository sets up your SSH configuration such that you only have to type your password on the first SSH connection. Any subsequent connections will reuse the first one as long as it's still open.

For this to work download the script and run it **once** as follows:
```
bash setup_ssh_control.sh
```

### Caveats
Connection sharing can interfere with port and/or X forwarding.
If you have issues, try closing all SSH connections to the same host.
You should be prompted for the password on the next connection.
If not, you forgot to close one.

## Introduction to Git
See `git_intro.pdf`

## HEP Software Foundation Training Center
The HSF has lots of resources on general as well as scientific computing.
Check out their training center under https://hepsoftwarefoundation.org/training/curriculum.html