#!/bin/bash

# Script to set up SSH config to reuse existing connections
# to reduce number of password prompts
# DG 2023/06/27


# Exit on error
set -eEuxo pipefail


ssh_folder="${HOME}/.ssh"
ssh_config="${ssh_folder}/config"
control_folder="${ssh_folder}/control"


mkdir -p "${control_folder}"
chmod 700 "${control_folder}"

(
cat << EOF

# Reuse existing connections to reduce number of password prompts
Host *
    ControlMaster auto
    ControlPath ${control_folder}/%r@%h:%p

EOF
) >> "${ssh_config}"
